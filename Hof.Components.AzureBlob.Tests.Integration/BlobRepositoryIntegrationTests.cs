﻿using System;
using System.IO;

using Hof.Components.Common.Interfaces;
using Moq;

using NUnit.Framework;

namespace Hof.Components.AzureBlob.Tests.Integration
{
    public class BlobRepositoryIntegrationTests
    {
        private const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}";
        private const string AccountName = "hofinttablestorage";
        private const string AccountKey = "g9fxFlokpcrqf9zsCz1fDDU0k/HuoEzR6cQ4HAJwEAu5BB7B3pnrlbxVI69aTsNsGhd+1JpRGabH/D0Gj4/qzw==";
        private const int RetryCount = 3;
        private const double DefaultWaitOnRetries = 2.0;
        private const double MaxWaitOnRetries = 10.0;

        private readonly ITelemetryClient mockTelemetryClient = new Mock<ITelemetryClient>().Object;
        private readonly IBlobStorageOperationRetry mockRetryConfiguration = Mock.Of<IBlobStorageOperationRetry>(m => m.RetryCount == RetryCount && m.DefaultWaitOnRetryInMs == DefaultWaitOnRetries && m.MaxWaitOnRetryInMs == MaxWaitOnRetries);

        public void Constructor_InvalidConnectionString_FormatExceptionThrown()
        {
            Assert.Throws<FormatException>(() => new BlobRepository("Hello World", mockTelemetryClient, mockRetryConfiguration));
        }

        [Test]
        public void Constructor_InvalidCredentials_FormatExceptionThrown()
        {
            var cs = string.Format(ConnectionString, "FOO", "BAR");
            Assert.Throws<FormatException>(() => new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration));
        }

        [Test]
        public void Constructor_ValidConnectionString_NoExceptionThrown()
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            Assert.DoesNotThrow(() => new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration));
        }

        [Test]
        public void CreateContainer_ValidArguments_NoExceptionThrown()
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            var repository = new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration);
            var containerName = GetContainerName();
            Assert.DoesNotThrow(() => repository.CreateContainer(containerName));
            DeleteContainer(containerName);
        }

        [Test]
        public void WriteFile_ValidArgumentsPath_FileWritten()
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            var repository = new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration);
            var containerName = GetContainerName();
            Assert.DoesNotThrow(() => repository.CreateContainer(containerName));
            Assert.DoesNotThrow(() => repository.Write(containerName, "TestFile.txt"));
            this.DeleteContainer(containerName);
        }

        [Test]
        public void WriteFile_ValidArgumentsBytes_FileWritten()
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            var repository = new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration);
            var containerName = GetContainerName();
            Assert.DoesNotThrow(() => repository.CreateContainer(containerName));
            Assert.DoesNotThrow(() => repository.Write(containerName, "Test2", File.ReadAllBytes("TestFile.txt")));
            this.DeleteContainer(containerName);
        }

        [Test]
        public void WriteFile_ValidArgumentsStream_FileWritten()
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            var repository = new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration);
            var containerName = GetContainerName();
            Assert.DoesNotThrow(() => repository.CreateContainer(containerName));
            using (var stream = File.OpenRead("TestFile.txt"))
            {
                Assert.DoesNotThrow(() => repository.Write(containerName, "Test3", stream));
            }

            this.DeleteContainer(containerName);
        }

        [Test]
        public void ReadFile_ValidArgumentsPath_FileRead()
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            var repository = new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration);
            var containerName = GetContainerName();
            Assert.DoesNotThrow(() => repository.CreateContainer(containerName));
            Assert.DoesNotThrow(() => repository.Write(containerName, "TestFile.txt"));
            byte[] bytes = repository.Read(containerName, "TestFile.txt");
            var newFile = Guid.NewGuid().ToString() + ".txt";
            File.WriteAllBytes(newFile, bytes);

            string x = File.ReadAllText(newFile);
            Assert.AreEqual("Hello World", x);

            this.DeleteContainer(containerName);
        }

        private void DeleteContainer(string containerName)
        {
            var cs = string.Format(ConnectionString, AccountName, AccountKey);
            var repository = new BlobRepository(cs, mockTelemetryClient, mockRetryConfiguration);
            repository.DeleteContainer(containerName);
        }

        private string GetContainerName()
        {
            return "test" + Guid.NewGuid().ToString().ToLower().Substring(0, 8);
        }
    }
}
