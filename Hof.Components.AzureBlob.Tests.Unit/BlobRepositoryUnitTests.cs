﻿using System;

using Hof.Components.Common.Interfaces;

using Moq;

using NUnit.Framework;

namespace Hof.Components.AzureBlob.Tests.Unit
{
    [TestFixture]
    public class BlobRepositoryUnitTests
    {
        private readonly ITelemetryClient mockTelemetryClient = new Mock<ITelemetryClient>().Object;
        private readonly IBlobStorageOperationRetry mockRetryConfiguration = new Mock<IBlobStorageOperationRetry>().Object;

        [Test]
        public void Constructor_NullConnectionString_ArgumentNullExceptionThrown()
        {
            Assert.Throws<ArgumentNullException>(() => new BlobRepository(null, mockTelemetryClient, mockRetryConfiguration));
        }

        [Test]
        public void Constructor_EmptyConnectionString_ArgumentNullExceptionThrown()
        {
            Assert.Throws<ArgumentNullException>(() => new BlobRepository(string.Empty, mockTelemetryClient, mockRetryConfiguration));
        }

        [Test]
        public void Constructor_WhitespaceConnectionString_ArgumentNullExceptionThrown()
        {
            Assert.Throws<ArgumentNullException>(() => new BlobRepository("     ", mockTelemetryClient, mockRetryConfiguration));
        }
    }
}
