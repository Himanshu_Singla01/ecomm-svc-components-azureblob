﻿using System;

namespace Hof.Components.AzureBlob
{
    /// <summary>
    /// Helper class containing sleep / wait providers for use in retry policies.
    /// </summary>
    public static class RetryWaitProviders
    {
        /// <summary>
        /// Provides a delay that increases exponentially according to the number of times it is called.
        /// </summary>
        public static readonly Func<int, double, double, TimeSpan> ExponentialBackoffProvider =
            (retryAttempt, baseDelayInMs, maxDelayInMs) =>
            TimeSpan.FromMilliseconds(Math.Min(maxDelayInMs, baseDelayInMs * Math.Pow(2, retryAttempt)));
    }
}
