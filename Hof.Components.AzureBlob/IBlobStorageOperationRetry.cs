﻿using Hof.Components.IO.Interfaces;

namespace Hof.Components.AzureBlob
{
    /// <summary>
    /// Represent the configuration required when apply a retry pattern to a file operation
    /// </summary>
    public interface IBlobStorageOperationRetry : IRetry
    {
    }
}
