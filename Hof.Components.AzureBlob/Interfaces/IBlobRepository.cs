﻿using System.IO;

using Microsoft.WindowsAzure.Storage.Blob;

namespace Hof.Components.AzureBlob.Interfaces
{
    /// <summary>
    /// Interface to define blob repository administration
    /// </summary>
    public interface IBlobRepository
    {
        /// <summary>
        /// Create a container.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="accessType">Type of access permission.</param>
        void CreateContainer(string containerName, BlobContainerPublicAccessType accessType = BlobContainerPublicAccessType.Blob);

        /// <summary>
        /// Delete a container.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        void DeleteContainer(string containerName);

        /// <summary>
        /// Writes a file to the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="filePath">The file path.</param>
        void Write(string containerName, string filePath);

        /// <summary>
        /// Writes a file to the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileBytes">The file bytes.</param>
        void Write(string containerName, string fileName, byte[] fileBytes);

        /// <summary>
        /// Writes a file to the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileStream">The file stream.</param>
        void Write(string containerName, string fileName, FileStream fileStream);

        /// <summary>
        /// Reads a file from the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>byte array of the file.</returns>
        byte[] Read(string containerName, string fileName);
    }
}
