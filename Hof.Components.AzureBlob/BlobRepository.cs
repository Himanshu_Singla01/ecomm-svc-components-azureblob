﻿using System;
using System.IO;

using Hof.Components.AzureBlob.Interfaces;
using Hof.Components.Common.Interfaces;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

using Polly;

namespace Hof.Components.AzureBlob
{
    /// <summary>
    /// Class to encapsulate Blob repository administration.
    /// </summary>
    public class BlobRepository : IBlobRepository
    {
        private readonly CloudBlobClient _blobClient;
        private readonly ITelemetryClient _telemetryClient;
        private readonly IBlobStorageOperationRetry _retryConfiguration;
        private readonly Policy policy;

        /// <summary> Initializes a new instance of the <see cref="BlobRepository"/> class. </summary>
        /// <param name="connectionString"> The connection string. </param>
        /// <param name="telemetryClient"> The telemetry Client. </param>
        /// <param name="retryConfiguration"> The retry configuration </param>
        public BlobRepository(string connectionString, ITelemetryClient telemetryClient, IBlobStorageOperationRetry retryConfiguration)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException("connectionString");
            }

            if (telemetryClient == null)
            {
                throw new ArgumentNullException("telemetryClient");
            }

            _telemetryClient = telemetryClient;
            _retryConfiguration = retryConfiguration;
             
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            _blobClient = storageAccount.CreateCloudBlobClient();

            // Set Policy to handle exceptions relating to not being able to access blob storage - for example, network connectivity issues.
            policy = Policy.Handle<StorageException>()
                .Or<IOException>()
                .WaitAndRetry(_retryConfiguration.RetryCount, count => RetryWaitProviders.ExponentialBackoffProvider(count, _retryConfiguration.DefaultWaitOnRetryInMs, _retryConfiguration.MaxWaitOnRetryInMs));
        }

        /// <summary>
        /// Create a container.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="accessType">Type of access permission.</param>
        public void CreateContainer(string containerName, BlobContainerPublicAccessType accessType = BlobContainerPublicAccessType.Blob)
        {
            var attemptNo = 0;
            try
            {
                policy.Execute(() =>
                {
                    ++attemptNo;
                    var container = _blobClient.GetContainerReference(containerName);
                    container.CreateIfNotExists(accessType);
                });
                _telemetryClient.TrackTrace(
                    string.Format("Created Container {0} after {1} attempt(s)", containerName, attemptNo));
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(string.Format("Failed to create Container {0}", containerName), ex);
            }
        }

        /// <summary>
        /// Delete a container.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        public void DeleteContainer(string containerName)
        {
            var attemptNo = 0;
            try
            {
                policy.Execute(() =>
                {
                    ++attemptNo;
                    var container = _blobClient.GetContainerReference(containerName);
                    container.DeleteIfExists();
                });
                _telemetryClient.TrackTrace(string.Format("Deleted Container {0} after {1} attempts", containerName, attemptNo));
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(string.Format("Error occurred Deleting container {0}", containerName), ex);
            }
        }

        /// <summary>
        /// Writes a file to the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="filePath">The file path.</param>
        public void Write(string containerName, string filePath)
        {
            var attemptNo = 0;
            var fileName = Path.GetFileName(filePath);
            try
            {
                policy.Execute(() =>
                {
                    ++attemptNo;
                    var container = _blobClient.GetContainerReference(containerName);
                    var blob = container.GetBlockBlobReference(fileName);
                    blob.UploadFromFile(filePath, AccessCondition.GenerateEmptyCondition());
                });
                _telemetryClient.TrackTrace(string.Format("Completed Writing file {0} to container {1} after {2} attempt(s)", fileName, containerName, attemptNo));
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(string.Format("Error occurred Writing file {0} to container {1}", fileName, containerName), ex);
            }
        }

        /// <summary>
        /// Writes a file to the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileBytes">The file bytes.</param>
        public void Write(string containerName, string fileName, byte[] fileBytes)
        {
            var attemptNo = 0;
            try
            {
                policy.Execute(() =>
                {
                    ++attemptNo;
                    var container = _blobClient.GetContainerReference(containerName);
                    var blob = container.GetBlockBlobReference(fileName);
                    blob.UploadFromByteArray(fileBytes, 0, fileBytes.Length);
                });
                _telemetryClient.TrackTrace(string.Format("Completed Writing file {0} to container {1} after {2} attempt(s)", fileName, containerName, attemptNo));
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(string.Format("Error occurred Writing file {0} to container {1}", fileName, containerName), ex);
            }
        }

        /// <summary>
        /// Writes a file to the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileStream">The file stream.</param>
        public void Write(string containerName, string fileName, FileStream fileStream)
        {
            var attemptNo = 0;
            try
            {
                policy.Execute(() =>
                {
                    ++attemptNo;
                    var container = _blobClient.GetContainerReference(containerName);
                    var blob = container.GetBlockBlobReference(fileName);
                    blob.UploadFromStream(fileStream);
                });
                _telemetryClient.TrackTrace(string.Format("Completed Writing file {0} to container {1} after {2} attempt(s)", fileName, containerName, attemptNo));
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(string.Format("Error occurred Writing file {0} to container {1}", fileName, containerName), ex);
            }
        }

        /// <summary>
        /// Reads a file from the the specified container name.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>byte array of the file.</returns>
        public byte[] Read(string containerName, string fileName)
        {
            var stream = new MemoryStream();
            var attemptNo = 0;
            try
            {
                policy.Execute(() =>
                {
                    ++attemptNo;
                    var container = _blobClient.GetContainerReference(containerName);
                    var blob = container.GetBlockBlobReference(fileName);
                    blob.DownloadToStream(stream);
                });
                _telemetryClient.TrackTrace(string.Format("Completed Reading file {0} from container {1} after {2} attempt(s)", fileName, containerName, attemptNo));
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(string.Format("Error occurred Reading file {0} from container {1}", fileName, containerName), ex);
            }

            return stream.ToArray();
        }
    }
}
