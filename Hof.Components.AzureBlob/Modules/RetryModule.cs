﻿using System.Configuration;

using Autofac;

using Castle.Components.DictionaryAdapter;

namespace Hof.Components.AzureBlob.Modules
{
    /// <summary>
    /// Registers dependencies required for the retry pattern
    /// </summary>
    public class RetryModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(new DictionaryAdapterFactory().GetAdapter<IBlobStorageOperationRetry>(ConfigurationManager.AppSettings));
        }
    }
}
